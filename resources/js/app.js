require('./bootstrap');
import { createApp } from 'vue'

import LeadTableComponent from './components/LeadTableComponent.vue';


const app = createApp({});
app.component('lead-table-component', LeadTableComponent)
    .mount('#app');
